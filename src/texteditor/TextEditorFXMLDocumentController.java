/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texteditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Guest
 */
public class TextEditorFXMLDocumentController implements Initializable {

    private Label label;

    private FileChooser fileChooser = new FileChooser();
    private File file;

    @FXML
    private MenuItem open;
    @FXML
    private MenuItem undo;
    @FXML
    private MenuItem selectAll;
    @FXML
    private MenuItem copy;
    @FXML
    private MenuItem cut;
    @FXML
    private MenuItem paste;
    @FXML
    private MenuItem delete;
    @FXML
    private MenuItem wordWrap;
    @FXML
    private MenuItem about;
    @FXML
    private TextArea textpane;
    @FXML
    private MenuItem saveAs;
    @FXML
    private MenuItem close;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void menuItemNew(ActionEvent event) {
        textpane.clear();
        Stage stage = (Stage) textpane.getScene().getWindow();
        stage.setTitle("Untitled - Notepad");
        file = null;
    }

    @FXML
    private void menuItemOpen(ActionEvent event) {
        textpane.clear();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        file = fileChooser.showOpenDialog(null);
        if (file != null) {
            Stage stage = (Stage) textpane.getScene().getWindow();
            stage.setTitle(file.getName() + " - TextEditor");
            BufferedReader br = null;
            try {
                String sCurrentLine;
                br = new BufferedReader(new FileReader(file));
                while ((sCurrentLine = br.readLine()) != null) {
                    textpane.appendText(sCurrentLine + "\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void menuItemSave(ActionEvent event) {
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        String content = textpane.getText();
        if (file != null) {
            try {
                // if file doesnt exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // open a file dialog box
            File file = fileChooser.showSaveDialog(null);
            if (file != null) {
                Stage stage = (Stage) textpane.getScene().getWindow();
                stage.setTitle(file.getName() + " - TextEditor");
                try {
                    // if file doesnt exists, then create it
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(content);
                    bw.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    private void menuItemSaveAs(ActionEvent event) {

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        file = fileChooser.showSaveDialog(null);

        String content = textpane.getText();
        if (file != null) {
            Stage stage = (Stage) textpane.getScene().getWindow();
            stage.setTitle(file.getName() + " - TextEditor");
            try {
                // if file doesnt exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void menuItemUndo(ActionEvent event) {
        textpane.undo();
    }

    @FXML
    private void menuItemSelectAll(ActionEvent event) {
        textpane.selectAll();

    }

    @FXML
    private void menuItemCopy(ActionEvent event) {
        textpane.copy();
    }

    @FXML
    private void menuItemCut(ActionEvent event) {
        textpane.cut();
    }

    @FXML
    private void menuItemPaste(ActionEvent event) {
        textpane.paste();
    }

    @FXML
    private void menuItemDelete(ActionEvent event) {
        textpane.clear();

    }

    @FXML
    private void menuItemWordWrap(ActionEvent event) {
        textpane.wrapTextProperty().set(true);
    }

    @FXML
    private void menuItemAbout(ActionEvent event) {
        Alert aboutAlert = new Alert(AlertType.INFORMATION);
        aboutAlert.setTitle("About TextEditor");
        aboutAlert.setHeaderText(null);
        aboutAlert.setContentText("TextEditor created by Tunde Aluko");

        aboutAlert.showAndWait();
    }

    @FXML
    private void menuItemClose(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void menuItemRedo(ActionEvent event) {
        textpane.redo();
    }

    @FXML
    private void menuItemFont1(ActionEvent event) {
        textpane.setFont(Font.font("serif"));
    }

    @FXML
    private void menuItemFont2(ActionEvent event) {
        textpane.setStyle("-fx-font-family: sans-serif");
    }

    @FXML
    private void menuItemFont3(ActionEvent event) {
        textpane.setStyle("-fx-font-family: cursive");
    }

    @FXML
    private void menuItemFont4(ActionEvent event) {
        textpane.setStyle("-fx-font-family: monospace");
    }

    @FXML
    private void menuItemBold(ActionEvent event) {
        textpane.setStyle("-fx-font-weight: bold");
    }

    @FXML
    private void menuItemItalic(ActionEvent event) {
        textpane.setStyle("-fx-font-style: italic");
    }

    @FXML
    private void menuItemSize18(ActionEvent event) {
        textpane.setStyle("-fx-font-size: 18");
    }

    @FXML
    private void menuItemSize24(ActionEvent event) {
        textpane.setStyle("-fx-font-size: 24");
    }

    @FXML
    private void menuItemSizeDefault(ActionEvent event) {
        textpane.setStyle("-fx-font-size: 12");
    }

}
